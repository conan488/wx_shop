//index.js
const API = require('../../API/API.js');
//获取应用实例
const app = getApp()
let goods1 = []

Page({
  data: {
    goods: [], //商品列表
  },
  //事件处理函数
  toDetail: function (e) {
    let detailItem = JSON.stringify(e.currentTarget.dataset.item);
    wx.navigateTo({
      url: '../../subPackage/pages/details/details?detailItem=' + encodeURIComponent(detailItem)
    })
  },
  // 分享
  toShare: function (e) {
    console.log("分享");
  },
  // 加入购物车
  addCart: function (e) {
    // 当前商品
    let cuObj = e.currentTarget.dataset.item;
    /**  加入购物车逻辑
    *遍历购物车数据，看看该商品是否已经加入购物车，
    *如果是，则数量 加1
    *否则，加入该商品
    */
    let goodsCarData = wx.getStorageSync("goodsCar") || [];
    if(goodsCarData.length > 0){
      let isOld = false;
      let oldItem = {};
      for(let i = 0; i < goodsCarData.length; i++){
        oldItem = goodsCarData[i];
        if(oldItem.id === cuObj.id){
          isOld = true;
          break;
        }
      }
      if(isOld){
        // 点击的商品在购物车已存在
        oldItem.count += 1;
        goodsCarData[goodsCarData.indexOf(oldItem)] = oldItem;
      }else{
        cuObj.count = 1;
        cuObj.selected = true;
        // 默认规格
        cuObj.selecteGuige = 1;
        goodsCarData.push(cuObj);
      }
    }else{
      cuObj.count = 1;
      cuObj.selected = true;
      // 默认规格
      cuObj.selecteGuige = 1;
      goodsCarData.push(cuObj);
    }
    try {
      wx.setStorageSync('goodsCar', goodsCarData);
      //弹框提示
      wx.showToast({
        title: '商品已成功加入购物车',
        icon: 'none',
        duration: 2000
      });
    } catch (e) {
      //弹框提示
      wx.showToast({
        title: '存储本地缓存数据失败，请检查相关配置，是否联网等',
        icon: 'none',
        duration: 2000
      });
    }
  },
  onLoad: function () {
    console.log('index onLoad');
    goods1 = [{
      imgGood: "http://aimg8.dlszyht.net.cn/module_pic_con/800_1500/1845135/5598/11195373_1551929870.jpg?x-oss-process=image/resize,m_lfit,w_360,h_240,limit_0",//商品图片地址
      nameGood: "生日节日访友水果礼篮.生日节日访友水果礼篮",//商品名称和描述
      nameDes: "送礼自用两相宜，一次订购两盒尊享包邮服务", //描述
      npriceGood: 239,//商品最新价格
      opriceGood: 298,//商品以往价格
      count: 0,//商品数量
      id: 0,//商品id
      selected: false,//商品是否为被选中状态，购物车默认全选
      specifications: [
        {
          guigeName: "500g随机搭配/箱",
          guigeId: 1
        }, 
        {
          guigeName: "促销享会员价 500元/3箱",
          guigeId: 2
        },
        {
          guigeName: "500g纯苹果/箱",
          guigeId: 3
        },
        {
          guigeName: "500g纯香蕉/箱",
          guigeId: 4
        },
      ],//商品规格
      imgUrls: [
        {
          url: 'https://vod.300hu.com/4c1f7a6atransbjngwcloud1oss/31174044181056509244399617/v.f30.mp4?dockingId=c8456f2f-15b7-4cfd-9e49-26005a06bf64&storageSource=3',
          type: 1
        },
        {
          url: 'http://f.hiphotos.baidu.com/image/h%3D300/sign=d985fb87d81b0ef473e89e5eedc551a1/b151f8198618367aa7f3cc7424738bd4b31ce525.jpg',
          type: 0
        },
        {
          url: 'http://a.hiphotos.baidu.com/image/pic/item/f603918fa0ec08fa3139e00153ee3d6d55fbda5f.jpg',
          type: 0
        },
        {
          url: 'http://b.hiphotos.baidu.com/image/pic/item/0eb30f2442a7d9337119f7dba74bd11372f001e0.jpg',
          type: 0
        }], //轮播
      isSale: "1000+"
    },]

  },
  onShow: function () {
    this.setData({
      goods: API.goods,
    })
  }
})
